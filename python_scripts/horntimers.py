#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Provides all timer classes needed for the UWRhorn module.
'''

import logging
import threading
import time
import math

Logger = logging.getLogger(__name__)

###############################################################################
class TimerThread (threading.Thread):
    ''' Timer which is started in its own thread. Can only be started once
    and dies if it is stopped. All active timers are in the class variable
    *__all__*. Before the program closes all these timer should be stopped,
    so that no running threads are left over.

    .. todo:: connect all timer threads to the main thread, so that thy are
        automatically stopped, when the main thread is stopped. Currently
        they are all stopped maually.
    '''

    def __init__(self, endTime, function, *args, **kwargs):
        '''
        :param int endTime: Time span after which *function* is called
            in seconds.
        :param callable function: This function is called when *endTime* is
            reached.
        '''
        super(TimerThread, self).__init__()
        self.daemon = True
        self.endTime = endTime
        self.stopped = threading.Event()
        self.startTime = 0
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def run(self):
        ''' Starts the timer.
        '''
        self.stopped.clear()
        self.startTime = time.time()
        self.stopped.wait(self.endTime)

        if not self.stopped.is_set():
            self.stopped.set()
            self.function(*self.args, **self.kwargs)

    def getTime(self):
        ''' Gives the time that has passed since the timer was started.

        :returns: passed time
        :rtype: float
        '''
        return time.time() - self.startTime


###############################################################################
class AbstractTimer():
    ''' Abstract class to hangle all the different time outputs.
    Children need to implement self.get_time and have the member variable
    self.time_length.
    '''

    def has_finished(self):
        ''' Checks if the timer has finished.

        :returns: TRUE if time is reached, else FALSE
        '''
        if self.get_time() >= self.time_length:
            return True
        else:
            return False

    def get_time(self):
        ''' Returns the current time of the timer.

        :returns: Count-up time of the timer.
        :rtype: float
        '''
        raise NotImplementedError("Please Implement this method")

    def get_time_string(self):
        ''' Returns the time left till the timer has finished as a string
        in the format "mm:ss"

        :retruns: Count-down time of timer in format "mm:ss"
        :rtype: str
        '''
        t = self.get_time()
        if t >= 0:
            t_min, t_sec = divmod(t, 60)
            t_min = int(t_min)
            t_sec = int(math.floor(t_sec))
            t_str = ' {:02d}:{:02d}'.format(t_min, t_sec)
        else:
            t_min, t_sec = divmod(-1*t, 60)
            t_min = int(t_min)
            t_sec = int(math.ceil(t_sec))
            if t_sec == 60:
                t_min += 1
                t_sec = 0
            t_str = '-{:02d}:{:02d}'.format(t_min, t_sec)
        return t_str

    def get_countdown_time(self):
        ''' Returns the time left till the timer has finished in seconds.

        :returns: Count-down time of timer in seconds.
        :rtype: float
        '''
        ct = self.time_length - self.get_time()
        return ct

    def get_countdown_time_string(self):
        ''' Returns the time left till the timer has finished as a string
        in the format "mm:ss"

        :retruns: Count-down time of timer in format "mm:ss"
        :rtype: str
        '''
        ct = self.get_countdown_time()
        if ct >= 0:
            min_down, sec_down = divmod(ct, 60)
            min_down = int(min_down)
            sec_down = int(math.ceil(sec_down))
            if sec_down == 60:
                min_down += 1
                sec_down = 0
            ct_str = ' {:02d}:{:02d}'.format(min_down, sec_down)
        else:
            min_down, sec_down = divmod(-1 * ct, 60)
            min_down = int(min_down)
            sec_down = int(math.floor(sec_down))
            ct_str = '-{:02d}:{:02d}'.format(min_down, sec_down)
        return ct_str

    def get_percentage_remainig(self):
        ''' Returns the percntage of time left on this timer.

        :returns: percentage left
        :rtype: int
        '''
        if not self.time_length == 0:
            ratio = self.get_countdown_time() / self.time_length
            return math.ceil(ratio * 10000) / 100
        else:
            return 0


###############################################################################
class Timer(AbstractTimer):
    ''' Creates a timer, which calls a function when a specified
      time is reached. It can be paused, restarted and reset.'''

    def __init__(self, time_length, function=None, *args, **kwargs):
        '''
        :param float time_length: Time span after which *function* is called in
            seconds.
        :param callable function: This function is called when *time_length* is
            reached.
        '''
        self.time_length = time_length
        self.elapsed_time = 0
        self.timer1 = None
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.continuous_time = False

    def finished(self):
        ''' Is called when the internal timer has run out. Calls the function
        specified in the construnctor.
        '''
        self.elapsed_time = self.time_length
        if self.function:
            self.function(*self.args, **self.kwargs)

    def start(self):
        ''' Starts the timer after it has been stopped or reset.
        '''
        if not self.is_running():
            time_diff = self.time_length - self.elapsed_time
            if time_diff > 0:  # i.e time_length > elapsed_time
                self.timer1 = TimerThread(time_diff, self.finished)
                self.timer1.start()

    def stop(self, force=False):
        ''' Stops the timer, if it is running already.
        '''
        if self.is_running():
            if not self.continuous_time or force:
                self.elapsed_time = self.elapsed_time + self.timer1.getTime()
                self.timer1.stopped.set()

    def reset(self):
        ''' Stops the timer and resets it to zero.
        '''
        if self.timer1:
            self.timer1.stopped.set()
        self.elapsed_time = 0

    def restart(self):
        ''' Resets the timer and starts it from the beginning.
        '''
        self.reset()
        self.start()

    def is_running(self):
        ''' Checks if the timer is running.

        :returns: TRUE if timer is running, else FALSE
        '''
        if self.timer1 is not None and not self.timer1.stopped.is_set():
            return True
        else:
            return False

    def get_time(self):
        ''' Returns the current time of the timer.

        :returns: Count-up time of the timer.
        :rtype: float
        '''
        if self.is_running():
            return self.elapsed_time + self.timer1.getTime()
        else:
            return self.elapsed_time

    def set_finished(self):
        ''' Sets the timer to it's finished state.'''
        if self.is_running():
            self.stop()
        self.elapsed_time = self.time_length

    def get_state(self):
        ''' Creates a dictionary with all important information of this class. This
        dictionary can be used to set this class back into this state.

        :returns: the state of this class
        :rtype: dict
        '''
        state = {"time_length": self.time_length,
                 "elapsed_time": self.get_time(),
                 "is_running": self.is_running()}
        return state

    def set_state(self, new_state):
        ''' Sets this class' state. This function needs to be treated with care,
        since no sanity check is performed before applying the new state.

        :param dict state: New state of this class.
        '''
        self.stop()
        if sorted(self.get_state().keys()) == sorted(new_state.keys()):
            self.time_length = float(new_state["time_length"])
            self.elapsed_time = float(new_state["elapsed_time"])
            if new_state["is_running"]:
                self.start()
        else:
            raise ValueError('Timer: this new state has not the correct keys')


###############################################################################
class CountUpTimer(AbstractTimer):
    ''' Creates a timer that only counts up. It has no total time length, when
    it ends automatically.
    '''

    def __init__(self):
        self.start_time = 0
        self.elapsed_time = 0
        self.time_length = 0    # This is used for the AddedTimers class.
        self._is_running = False

    def is_running(self):
        ''' Checks if the timer is running.

        :returns: TRUE if timer is running, else FALSE
        '''
        return self._is_running

    def start(self):
        ''' Starts the timer after it has been stopped or reset.
        '''
        if not self.is_running():
            self.start_time = time.time()
            self._is_running = True

    def stop(self):
        ''' Stops the timer, if it is running already.
        '''
        if self.is_running():
            self.elapsed_time = time.time() - self.start_time
            self._is_running = False

    def reset(self):
        ''' Stops the timer and resets it to zero.
        '''
        self.start_time = 0
        self.elapsed_time = 0
        self._is_running = False

    def restart(self):
        ''' Resets the timer and starts it from the beginning.
        '''
        self.reset()
        self.start()

    def get_time(self):
        ''' Returns the current time of the timer.

        :returns: Count-up time of the timer.
        :rtype: float
        '''
        if self.is_running():
            return self.elapsed_time + (time.time() - self.start_time)
        else:
            return self.elapsed_time

    def get_state(self):
        ''' Creates a dictionary with all important information of this class. This
        dictionary can be used to set this class back into this state.

        :returns: the state of this class
        :rtype: dict
        '''
        state = {"elapsed_time": self.get_time(),
                 "is_running": self.is_running()}
        return state

    def set_state(self, state):
        ''' Sets this class' state. This function needs to be treated with care,
        since no sanity check is performed before applying the new state.

        :param dict state: New state of this class.
        '''
        self.stop()
        if sorted(self.get_state().keys()) == sorted(state.keys()):
            self.start_time = 0
            self._is_running = False
            self.elapsed_time = float(state["elapsed_time"])
            if state["is_running"]:
                self.start()
        else:
            raise ValueError('Timer: this new state has not the correct keys')


###############################################################################
class LinkedTimer(AbstractTimer):

    def __init__(self, id, time_length, start_time, total_game_timer,
                 teamcolor='',
                 active=True):
        self.id = id
        self.time_length = time_length
        self.start_time = start_time
        self.total_game_timer = total_game_timer
        self.teamcolor = teamcolor
        self.active = active

    def get_time(self):
        time_passed = self.total_game_timer.get_time() - self.start_time
        if time_passed >= self.time_length:
            return self.time_length
        else:
            return time_passed

    def is_running(self):
        if self.get_time() >= self.time_length:
            return False
        else:
            return self.total_game_timer.is_running()

    def get_state(self):
        state = {"id": self.id,
                 "time_length": self.time_length,
                 "start_time": self.start_time,
                 "teamcolor": self.teamcolor,
                 "active": self.active}
        return state

    def set_state(self, new_state):
        if sorted(self.get_state().keys()) == sorted(new_state.keys()):
            self.time_length = new_state["time_length"]
            self.start_time = new_state["start_time"]
            self.teamcolor = new_state["teamcolor"]
            self.active = new_state["active"]
        else:
            raise ValueError('Timer: this new state has not the correct keys')

    def prepare_for_next_halftime(self):
        self.start_time = -1 * self.get_time()


###############################################################################
class AddedTimers(AbstractTimer):

    def __init__(self, timer1, timer2):
        self.timer1 = timer1
        self.timer2 = timer2

    @property
    def time_length(self):
        return self.timer1.time_length + self.timer2.time_length

    def get_time(self):
        return self.timer1.get_time() + self.timer2.get_time()

    def is_running(self):
        if self.timer1.is_running() or self.timer2.is_running():
            return True
        else:
            return False
