EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "Breakout Board"
Date ""
Rev "v 2.0"
Comp "Underwater Rugby Horn"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5700 2550 2    60   ~ 0
GPIO04
Text Label 5700 2750 2    60   ~ 0
GPIO17
Text Label 5700 2850 2    60   ~ 0
GPIO27
Text Label 5700 2950 2    60   ~ 0
GPIO22
$Comp
L Underwaterhorn-rescue:C C1
U 1 1 57EECB8D
P 2900 1950
F 0 "C1" H 2925 2050 50  0000 L CNN
F 1 "100n" H 2925 1850 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2938 1800 50  0001 C CNN
F 3 "" H 2900 1950 50  0000 C CNN
	1    2900 1950
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:C C2
U 1 1 57EED0F9
P 2900 3100
F 0 "C2" H 2925 3200 50  0000 L CNN
F 1 "100n" H 2925 3000 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2938 2950 50  0001 C CNN
F 3 "" H 2900 3100 50  0000 C CNN
	1    2900 3100
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:C C3
U 1 1 57EED141
P 2900 4250
F 0 "C3" H 2925 4350 50  0000 L CNN
F 1 "100n" H 2925 4150 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2938 4100 50  0001 C CNN
F 3 "" H 2900 4250 50  0000 C CNN
	1    2900 4250
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:R R1
U 1 1 57EED96B
P 2650 1700
F 0 "R1" V 2730 1700 50  0000 C CNN
F 1 "10k" V 2650 1700 50  0000 C CNN
F 2 "Resistors_THT:Resistor_Horizontal_RM10mm" V 2580 1700 50  0001 C CNN
F 3 "" H 2650 1700 50  0000 C CNN
	1    2650 1700
	0    1    -1   0   
$EndComp
$Comp
L Underwaterhorn-rescue:R R2
U 1 1 57EEDD29
P 2400 1950
F 0 "R2" V 2480 1950 50  0000 C CNN
F 1 "1k" V 2400 1950 50  0000 C CNN
F 2 "Resistors_THT:Resistor_Horizontal_RM10mm" V 2330 1950 50  0001 C CNN
F 3 "" H 2400 1950 50  0000 C CNN
	1    2400 1950
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:R R5
U 1 1 57EEE21D
P 2650 4000
F 0 "R5" V 2730 4000 50  0000 C CNN
F 1 "10k" V 2650 4000 50  0000 C CNN
F 2 "Resistors_THT:Resistor_Horizontal_RM10mm" V 2580 4000 50  0001 C CNN
F 3 "" H 2650 4000 50  0000 C CNN
	1    2650 4000
	0    1    -1   0   
$EndComp
$Comp
L Underwaterhorn-rescue:R R6
U 1 1 57EEE223
P 2400 4250
F 0 "R6" V 2480 4250 50  0000 C CNN
F 1 "1k" V 2400 4250 50  0000 C CNN
F 2 "Resistors_THT:Resistor_Horizontal_RM10mm" V 2330 4250 50  0001 C CNN
F 3 "" H 2400 4250 50  0000 C CNN
	1    2400 4250
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:R R3
U 1 1 57EEE6D6
P 2650 2850
F 0 "R3" V 2730 2850 50  0000 C CNN
F 1 "10k" V 2650 2850 50  0000 C CNN
F 2 "Resistors_THT:Resistor_Horizontal_RM10mm" V 2580 2850 50  0001 C CNN
F 3 "" H 2650 2850 50  0000 C CNN
	1    2650 2850
	0    1    -1   0   
$EndComp
$Comp
L Underwaterhorn-rescue:R R4
U 1 1 57EEE6DC
P 2400 3100
F 0 "R4" V 2480 3100 50  0000 C CNN
F 1 "1k" V 2400 3100 50  0000 C CNN
F 2 "Resistors_THT:Resistor_Horizontal_RM10mm" V 2330 3100 50  0001 C CNN
F 3 "" H 2400 3100 50  0000 C CNN
	1    2400 3100
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:4N25 U1
U 1 1 57EEF11E
P 2050 6750
F 0 "U1" H 1850 6950 50  0000 L CNN
F 1 "4N25" H 2050 6950 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 1850 6550 50  0001 L CIN
F 3 "" H 2050 6750 50  0000 L CNN
	1    2050 6750
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:R R7
U 1 1 57EF0B1F
P 1400 6650
F 0 "R7" V 1480 6650 50  0000 C CNN
F 1 "90" V 1400 6650 50  0000 C CNN
F 2 "Resistors_THT:Resistor_Horizontal_RM10mm" V 1330 6650 50  0001 C CNN
F 3 "" H 1400 6650 50  0000 C CNN
	1    1400 6650
	0    1    1    0   
$EndComp
$Comp
L Underwaterhorn-rescue:CONN_01X02 P1
U 1 1 57EF0F76
P 1550 1650
F 0 "P1" H 1550 1800 50  0000 C CNN
F 1 "Main Ref" V 1650 1650 50  0000 C CNN
F 2 "Connectors:PINHEAD1-2" H 1550 1650 50  0001 C CNN
F 3 "" H 1550 1650 50  0000 C CNN
	1    1550 1650
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:CONN_01X02 P3
U 1 1 57EF1A5D
P 1550 3950
F 0 "P3" H 1550 4100 50  0000 C CNN
F 1 "UW Ref 2" V 1650 3950 50  0000 C CNN
F 2 "Connectors:PINHEAD1-2" H 1550 3950 50  0001 C CNN
F 3 "" H 1550 3950 50  0000 C CNN
	1    1550 3950
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:CONN_01X02 P2
U 1 1 57EF1925
P 1550 2800
F 0 "P2" H 1550 2950 50  0000 C CNN
F 1 "UW Ref 1" V 1650 2800 50  0000 C CNN
F 2 "Connectors:PINHEAD1-2" H 1550 2800 50  0001 C CNN
F 3 "" H 1550 2800 50  0000 C CNN
	1    1550 2800
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:CONN_01X02 P4
U 1 1 57EF2FB5
P 5150 6300
F 0 "P4" H 5150 6450 50  0000 C CNN
F 1 "12 V" V 5250 6300 50  0000 C CNN
F 2 "Connectors:bornier2" H 5150 6300 50  0001 C CNN
F 3 "" H 5150 6300 50  0000 C CNN
	1    5150 6300
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:CONN_01X02 P6
U 1 1 57EF40F6
P 4050 5300
F 0 "P6" H 4050 5450 50  0000 C CNN
F 1 "Hupe 2" V 4150 5300 50  0000 C CNN
F 2 "Connectors:bornier2" H 4050 5300 50  0001 C CNN
F 3 "" H 4050 5300 50  0000 C CNN
	1    4050 5300
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:CONN_01X02 P7
U 1 1 57EF41AC
P 4600 5300
F 0 "P7" H 4600 5450 50  0000 C CNN
F 1 "Hupe 3" V 4700 5300 50  0000 C CNN
F 2 "Connectors:bornier2" H 4600 5300 50  0001 C CNN
F 3 "" H 4600 5300 50  0000 C CNN
	1    4600 5300
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:CONN_01X02 P5
U 1 1 57EF422A
P 3500 5300
F 0 "P5" H 3500 5450 50  0000 C CNN
F 1 "Hupe 1" V 3600 5300 50  0000 C CNN
F 2 "Connectors:bornier2" H 3500 5300 50  0001 C CNN
F 3 "" H 3500 5300 50  0000 C CNN
	1    3500 5300
	1    0    0    -1  
$EndComp
Text Label 4950 6250 2    60   ~ 0
+
Text Label 4950 6350 2    60   ~ 0
-
$Comp
L power:GND #PWR01
U 1 1 586FF7EE
P 2400 2100
F 0 "#PWR01" H 2400 1850 50  0001 C CNN
F 1 "GND" H 2400 1950 50  0000 C CNN
F 2 "" H 2400 2100 50  0000 C CNN
F 3 "" H 2400 2100 50  0000 C CNN
	1    2400 2100
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 586FF89C
P 2900 3250
F 0 "#PWR02" H 2900 3000 50  0001 C CNN
F 1 "GND" H 2900 3100 50  0000 C CNN
F 2 "" H 2900 3250 50  0000 C CNN
F 3 "" H 2900 3250 50  0000 C CNN
	1    2900 3250
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 586FF976
P 2400 3250
F 0 "#PWR03" H 2400 3000 50  0001 C CNN
F 1 "GND" H 2400 3100 50  0000 C CNN
F 2 "" H 2400 3250 50  0000 C CNN
F 3 "" H 2400 3250 50  0000 C CNN
	1    2400 3250
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 586FFA0E
P 2900 4400
F 0 "#PWR04" H 2900 4150 50  0001 C CNN
F 1 "GND" H 2900 4250 50  0000 C CNN
F 2 "" H 2900 4400 50  0000 C CNN
F 3 "" H 2900 4400 50  0000 C CNN
	1    2900 4400
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 586FFA7A
P 2400 4400
F 0 "#PWR05" H 2400 4150 50  0001 C CNN
F 1 "GND" H 2400 4250 50  0000 C CNN
F 2 "" H 2400 4400 50  0000 C CNN
F 3 "" H 2400 4400 50  0000 C CNN
	1    2400 4400
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:D D4
U 1 1 5870045B
P 1900 1950
F 0 "D4" H 1900 2050 50  0000 C CNN
F 1 "1N4148" H 1900 1850 50  0000 C CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 1900 1950 50  0001 C CNN
F 3 "" H 1900 1950 50  0000 C CNN
	1    1900 1950
	0    -1   -1   0   
$EndComp
$Comp
L Underwaterhorn-rescue:D D5
U 1 1 58700526
P 900 1600
F 0 "D5" H 900 1700 50  0000 C CNN
F 1 "1N4148" H 900 1500 50  0000 C CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 900 1600 50  0001 C CNN
F 3 "" H 900 1600 50  0000 C CNN
	1    900  1600
	0    -1   -1   0   
$EndComp
$Comp
L Underwaterhorn-rescue:D D2
U 1 1 587005CB
P 1900 4250
F 0 "D2" H 1900 4350 50  0000 C CNN
F 1 "1N4148" H 1900 4150 50  0000 C CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 1900 4250 50  0001 C CNN
F 3 "" H 1900 4250 50  0000 C CNN
	1    1900 4250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 58701EA5
P 1600 6950
F 0 "#PWR06" H 1600 6700 50  0001 C CNN
F 1 "GND" H 1600 6800 50  0000 C CNN
F 2 "" H 1600 6950 50  0000 C CNN
F 3 "" H 1600 6950 50  0000 C CNN
	1    1600 6950
	1    0    0    -1  
$EndComp
NoConn ~ 2350 6650
$Comp
L power:GND #PWR07
U 1 1 586FF740
P 2900 2100
F 0 "#PWR07" H 2900 1850 50  0001 C CNN
F 1 "GND" H 2900 1950 50  0000 C CNN
F 2 "" H 2900 2100 50  0000 C CNN
F 3 "" H 2900 2100 50  0000 C CNN
	1    2900 2100
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG08
U 1 1 58B0780C
P 6700 1350
F 0 "#FLG08" H 6700 1445 50  0001 C CNN
F 1 "PWR_FLAG" H 6700 1530 50  0000 C CNN
F 2 "" H 6700 1350 50  0000 C CNN
F 3 "" H 6700 1350 50  0000 C CNN
	1    6700 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 58B078F2
P 6700 1350
F 0 "#PWR09" H 6700 1100 50  0001 C CNN
F 1 "GND" H 6700 1200 50  0000 C CNN
F 2 "" H 6700 1350 50  0000 C CNN
F 3 "" H 6700 1350 50  0000 C CNN
	1    6700 1350
	1    0    0    -1  
$EndComp
NoConn ~ 3650 5750
$Comp
L Underwaterhorn-rescue:BC547 Q1
U 1 1 57EF0CA9
P 2850 6850
F 0 "Q1" H 3050 6925 50  0000 L CNN
F 1 "BC547" H 3050 6850 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 3050 6775 50  0001 L CIN
F 3 "" H 2850 6850 50  0000 L CNN
	1    2850 6850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR010
U 1 1 58CA7EC9
P 6100 1300
F 0 "#PWR010" H 6100 1150 50  0001 C CNN
F 1 "+3.3V" H 6100 1440 50  0000 C CNN
F 2 "" H 6100 1300 50  0000 C CNN
F 3 "" H 6100 1300 50  0000 C CNN
	1    6100 1300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG011
U 1 1 58CA7F19
P 6100 1300
F 0 "#FLG011" H 6100 1395 50  0001 C CNN
F 1 "PWR_FLAG" H 6100 1480 50  0000 C CNN
F 2 "" H 6100 1300 50  0000 C CNN
F 3 "" H 6100 1300 50  0000 C CNN
	1    6100 1300
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR012
U 1 1 58CA8137
P 1750 1450
F 0 "#PWR012" H 1750 1300 50  0001 C CNN
F 1 "+3.3V" H 1750 1590 50  0000 C CNN
F 2 "" H 1750 1450 50  0000 C CNN
F 3 "" H 1750 1450 50  0000 C CNN
	1    1750 1450
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR013
U 1 1 58CA8187
P 1750 2600
F 0 "#PWR013" H 1750 2450 50  0001 C CNN
F 1 "+3.3V" H 1750 2740 50  0000 C CNN
F 2 "" H 1750 2600 50  0000 C CNN
F 3 "" H 1750 2600 50  0000 C CNN
	1    1750 2600
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR014
U 1 1 58CA832B
P 1750 3750
F 0 "#PWR014" H 1750 3600 50  0001 C CNN
F 1 "+3.3V" H 1750 3890 50  0000 C CNN
F 2 "" H 1750 3750 50  0000 C CNN
F 3 "" H 1750 3750 50  0000 C CNN
	1    1750 3750
	-1   0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:RM50-xx11 RL1
U 1 1 585875BE
P 3550 6050
F 0 "RL1" H 4000 6200 50  0000 L CNN
F 1 "FRA2C" H 4000 6100 50  0000 L CNN
F 2 "custom_footprints:Relay_FRA2C" H 3550 6050 50  0001 C CNN
F 3 "" H 3550 6050 50  0000 C CNN
	1    3550 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4000 2900 4100
Wire Wire Line
	2800 4000 2900 4000
Connection ~ 2900 4000
Wire Wire Line
	2950 6200 2950 6450
Connection ~ 2950 6450
Wire Wire Line
	4950 5950 4950 6250
Wire Wire Line
	3850 4950 3850 5250
Connection ~ 3850 4950
Wire Wire Line
	4400 4950 4400 5250
Wire Wire Line
	2950 4950 2950 5700
Connection ~ 2950 5700
Wire Wire Line
	3300 5350 3300 5550
Wire Wire Line
	2500 6750 2350 6750
Wire Wire Line
	3300 4950 3300 5250
Wire Wire Line
	2500 6450 2500 6750
Connection ~ 3300 4950
Wire Wire Line
	2950 7150 2950 7050
Wire Wire Line
	2800 1700 2900 1700
Wire Wire Line
	2900 1700 2900 1800
Wire Wire Line
	2800 2850 2900 2850
Wire Wire Line
	2900 2850 2900 2950
Connection ~ 2900 2850
Wire Wire Line
	1750 2850 1900 2850
Wire Wire Line
	2400 2850 2400 2950
Wire Wire Line
	1750 4000 1900 4000
Wire Wire Line
	2400 4000 2400 4100
Wire Wire Line
	1750 1700 1900 1700
Wire Wire Line
	2400 1700 2400 1800
Connection ~ 2400 1700
Connection ~ 2400 2850
Connection ~ 1900 2850
Connection ~ 2400 4000
Wire Wire Line
	1550 6650 1750 6650
Wire Wire Line
	3300 5550 3850 5550
Wire Wire Line
	4400 5550 4400 5350
Wire Wire Line
	3850 5350 3850 5550
Connection ~ 3850 5550
Wire Wire Line
	2350 6850 2650 6850
Wire Wire Line
	1750 6850 1600 6850
Wire Wire Line
	1600 6850 1600 6950
Wire Wire Line
	3350 5700 3350 5750
Wire Wire Line
	3350 6450 3350 6350
Wire Wire Line
	3750 7150 3750 6350
Wire Wire Line
	2950 5700 3350 5700
Wire Wire Line
	2500 6450 2950 6450
Wire Wire Line
	1750 1600 1750 1450
Wire Wire Line
	1750 2750 1750 2600
Wire Wire Line
	1900 1700 1900 1800
Wire Wire Line
	1750 3750 1750 3900
Connection ~ 1900 1700
Wire Wire Line
	1900 2850 1900 2950
Connection ~ 1900 4000
Wire Wire Line
	1900 4000 1900 4100
Wire Wire Line
	3350 2750 5700 2750
Wire Wire Line
	3350 1700 3350 2750
Connection ~ 2900 1700
Wire Wire Line
	5700 2950 3350 2950
Wire Wire Line
	3350 2950 3350 4000
Wire Wire Line
	5700 2550 3550 2550
Wire Wire Line
	3550 2550 3550 1050
Wire Wire Line
	3550 1050 900  1050
Wire Wire Line
	900  1050 900  1450
$Comp
L Underwaterhorn-rescue:D D3
U 1 1 58700338
P 1900 3100
F 0 "D3" H 1900 3200 50  0000 C CNN
F 1 "1N4148" H 1900 3000 50  0000 C CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 1900 3100 50  0001 C CNN
F 3 "" H 1900 3100 50  0000 C CNN
	1    1900 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1900 2100 1900 2200
Wire Wire Line
	1900 2200 900  2200
Connection ~ 900  2200
Wire Wire Line
	1900 3250 1900 3350
Wire Wire Line
	900  3350 1900 3350
Connection ~ 900  3350
Wire Wire Line
	1900 4400 1900 4500
Wire Wire Line
	900  4500 1900 4500
Connection ~ 900  4500
$Comp
L power:+3.3V #PWR015
U 1 1 58CBB7B8
P 5700 2250
F 0 "#PWR015" H 5700 2100 50  0001 C CNN
F 1 "+3.3V" H 5700 2390 50  0000 C CNN
F 2 "" H 5700 2250 50  0000 C CNN
F 3 "" H 5700 2250 50  0000 C CNN
	1    5700 2250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR016
U 1 1 58CBB80A
P 5350 2650
F 0 "#PWR016" H 5350 2400 50  0001 C CNN
F 1 "GND" H 5350 2500 50  0000 C CNN
F 2 "" H 5350 2650 50  0000 C CNN
F 3 "" H 5350 2650 50  0000 C CNN
	1    5350 2650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 58CBBBC8
P 5700 3450
F 0 "#PWR017" H 5700 3200 50  0001 C CNN
F 1 "GND" H 5700 3300 50  0000 C CNN
F 2 "" H 5700 3450 50  0000 C CNN
F 3 "" H 5700 3450 50  0000 C CNN
	1    5700 3450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 58CBBC5E
P 5700 4150
F 0 "#PWR018" H 5700 3900 50  0001 C CNN
F 1 "GND" H 5700 4000 50  0000 C CNN
F 2 "" H 5700 4150 50  0000 C CNN
F 3 "" H 5700 4150 50  0000 C CNN
	1    5700 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 2650 5350 2650
$Comp
L power:GND #PWR019
U 1 1 58CBC054
P 7100 2450
F 0 "#PWR019" H 7100 2200 50  0001 C CNN
F 1 "GND" H 7100 2300 50  0000 C CNN
F 2 "" H 7100 2450 50  0000 C CNN
F 3 "" H 7100 2450 50  0000 C CNN
	1    7100 2450
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 58CBC17D
P 7100 2850
F 0 "#PWR020" H 7100 2600 50  0001 C CNN
F 1 "GND" H 7100 2700 50  0000 C CNN
F 2 "" H 7100 2850 50  0000 C CNN
F 3 "" H 7100 2850 50  0000 C CNN
	1    7100 2850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 58CBC37D
P 7100 3150
F 0 "#PWR021" H 7100 2900 50  0001 C CNN
F 1 "GND" H 7100 3000 50  0000 C CNN
F 2 "" H 7100 3150 50  0000 C CNN
F 3 "" H 7100 3150 50  0000 C CNN
	1    7100 3150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR022
U 1 1 58CBC3E5
P 7100 3650
F 0 "#PWR022" H 7100 3400 50  0001 C CNN
F 1 "GND" H 7100 3500 50  0000 C CNN
F 2 "" H 7100 3650 50  0000 C CNN
F 3 "" H 7100 3650 50  0000 C CNN
	1    7100 3650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR023
U 1 1 58CBC44D
P 7100 3850
F 0 "#PWR023" H 7100 3600 50  0001 C CNN
F 1 "GND" H 7100 3700 50  0000 C CNN
F 2 "" H 7100 3850 50  0000 C CNN
F 3 "" H 7100 3850 50  0000 C CNN
	1    7100 3850
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR024
U 1 1 58CBC4F8
P 5700 3050
F 0 "#PWR024" H 5700 2900 50  0001 C CNN
F 1 "+3.3V" H 5700 3190 50  0000 C CNN
F 2 "" H 5700 3050 50  0000 C CNN
F 3 "" H 5700 3050 50  0000 C CNN
	1    5700 3050
	0    -1   -1   0   
$EndComp
NoConn ~ 5700 3150
NoConn ~ 5700 3250
NoConn ~ 5700 3350
NoConn ~ 5700 3750
NoConn ~ 5700 3850
NoConn ~ 5700 3950
NoConn ~ 5700 4050
NoConn ~ 7100 4150
NoConn ~ 7100 4050
NoConn ~ 7100 3950
NoConn ~ 7100 3750
NoConn ~ 7100 3350
NoConn ~ 7100 3250
NoConn ~ 7100 3050
NoConn ~ 7100 2950
NoConn ~ 7100 2750
NoConn ~ 7100 2650
NoConn ~ 7100 2550
NoConn ~ 7100 2350
NoConn ~ 7100 2250
$Comp
L Underwaterhorn-rescue:CONN_01X02 P9
U 1 1 58CBDC4D
P 5800 6300
F 0 "P9" H 5800 6450 50  0000 C CNN
F 1 "12 V" V 5900 6300 50  0000 C CNN
F 2 "Connectors:bornier2" H 5800 6300 50  0001 C CNN
F 3 "" H 5800 6300 50  0000 C CNN
	1    5800 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 5950 5600 6250
Wire Wire Line
	5600 6600 5600 6350
Wire Wire Line
	4950 6600 5300 6600
Text Label 5600 6250 2    60   ~ 0
+
Text Label 5600 6350 2    60   ~ 0
-
NoConn ~ 5700 3650
$Comp
L custom_addons:Adafruit_PCF8523_RTC RTC1
U 1 1 58CC1AAF
P 4800 1900
F 0 "RTC1" H 5000 1650 66  0000 C CNN
F 1 "Adafruit_PCF8523_RTC" H 5050 2650 66  0000 C CNN
F 2 "custom_footprints:Adafruit_PCF8523" H 5450 2150 66  0001 C CNN
F 3 "" H 5450 2150 66  0001 C CNN
	1    4800 1900
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR025
U 1 1 58CC1CD8
P 5000 1350
F 0 "#PWR025" H 5000 1100 50  0001 C CNN
F 1 "GND" H 5000 1200 50  0000 C CNN
F 2 "" H 5000 1350 50  0000 C CNN
F 3 "" H 5000 1350 50  0000 C CNN
	1    5000 1350
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR026
U 1 1 58CC1E6A
P 5200 1500
F 0 "#PWR026" H 5200 1350 50  0001 C CNN
F 1 "+3.3V" H 5200 1640 50  0000 C CNN
F 2 "" H 5200 1500 50  0000 C CNN
F 3 "" H 5200 1500 50  0000 C CNN
	1    5200 1500
	0    1    1    0   
$EndComp
NoConn ~ 5000 1950
Wire Wire Line
	5000 1800 5150 1800
Wire Wire Line
	5150 1800 5150 2450
Wire Wire Line
	5150 2450 5700 2450
Wire Wire Line
	5000 1650 5250 1650
Wire Wire Line
	5250 1650 5250 2350
Wire Wire Line
	5250 2350 5700 2350
NoConn ~ 7100 3450
$Comp
L custom_addons:RaspberryPi3_GPIO RPi3_1
U 1 1 58CC3962
P 6400 3200
F 0 "RPi3_1" H 6400 4300 50  0000 C CNN
F 1 "RaspberryPi3_GPIO" H 6400 2100 50  0000 C CNN
F 2 "Connectors:IDC_Header_Straight_40pins" H 6450 2250 50  0001 C CNN
F 3 "" H 6450 2250 50  0000 C CNN
	1    6400 3200
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:D_Zener D1
U 1 1 58DD04AB
P 2950 6050
F 0 "D1" H 2950 6150 50  0000 C CNN
F 1 "D_Zener 16V" H 2950 5950 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2950 6050 50  0001 C CNN
F 3 "" H 2950 6050 50  0000 C CNN
	1    2950 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 4950 3300 4950
Wire Wire Line
	2950 7150 3350 7150
Wire Wire Line
	4950 5950 5300 5950
Wire Wire Line
	4950 6600 4950 6350
$Comp
L power:+12V #PWR027
U 1 1 58F4FD29
P 5300 5950
F 0 "#PWR027" H 5300 5800 50  0001 C CNN
F 1 "+12V" H 5300 6090 50  0000 C CNN
F 2 "" H 5300 5950 50  0001 C CNN
F 3 "" H 5300 5950 50  0001 C CNN
	1    5300 5950
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG028
U 1 1 58F4FFF5
P 6600 6250
F 0 "#FLG028" H 6600 6325 50  0001 C CNN
F 1 "PWR_FLAG" H 6600 6400 50  0000 C CNN
F 2 "" H 6600 6250 50  0001 C CNN
F 3 "" H 6600 6250 50  0001 C CNN
	1    6600 6250
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR029
U 1 1 58F50043
P 6600 6250
F 0 "#PWR029" H 6600 6100 50  0001 C CNN
F 1 "+12V" H 6600 6390 50  0000 C CNN
F 2 "" H 6600 6250 50  0001 C CNN
F 3 "" H 6600 6250 50  0001 C CNN
	1    6600 6250
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG030
U 1 1 58F50091
P 7100 6250
F 0 "#FLG030" H 7100 6325 50  0001 C CNN
F 1 "PWR_FLAG" H 7100 6400 50  0000 C CNN
F 2 "" H 7100 6250 50  0001 C CNN
F 3 "" H 7100 6250 50  0001 C CNN
	1    7100 6250
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:GNDPWR #PWR031
U 1 1 58F500DF
P 7100 6250
F 0 "#PWR031" H 7100 6050 50  0001 C CNN
F 1 "GNDPWR" H 7100 6120 50  0000 C CNN
F 2 "" H 7100 6200 50  0001 C CNN
F 3 "" H 7100 6200 50  0001 C CNN
	1    7100 6250
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:GNDPWR #PWR032
U 1 1 58F5012D
P 5300 6600
F 0 "#PWR032" H 5300 6400 50  0001 C CNN
F 1 "GNDPWR" H 5300 6470 50  0000 C CNN
F 2 "" H 5300 6550 50  0001 C CNN
F 3 "" H 5300 6550 50  0001 C CNN
	1    5300 6600
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:GNDPWR #PWR033
U 1 1 58F50263
P 3350 7150
F 0 "#PWR033" H 3350 6950 50  0001 C CNN
F 1 "GNDPWR" H 3350 7020 50  0000 C CNN
F 2 "" H 3350 7100 50  0001 C CNN
F 3 "" H 3350 7100 50  0001 C CNN
	1    3350 7150
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR034
U 1 1 58F50308
P 3850 4950
F 0 "#PWR034" H 3850 4800 50  0001 C CNN
F 1 "+12V" H 3850 5090 50  0000 C CNN
F 2 "" H 3850 4950 50  0001 C CNN
F 3 "" H 3850 4950 50  0001 C CNN
	1    3850 4950
	1    0    0    -1  
$EndComp
$Comp
L custom_addons:LM2678T-5,0 U2
U 1 1 58F5157E
P 2550 8350
F 0 "U2" H 2450 8650 66  0000 C CNN
F 1 "LM2678T-5,0" H 2450 7850 66  0000 C CNN
F 2 "" H 2750 8450 66  0001 C CNN
F 3 "" H 2750 8450 66  0001 C CNN
	1    2550 8350
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:GNDPWR #PWR035
U 1 1 58F51A37
P 6100 8700
F 0 "#PWR035" H 6100 8500 50  0001 C CNN
F 1 "GNDPWR" H 6100 8570 50  0000 C CNN
F 2 "" H 6100 8650 50  0001 C CNN
F 3 "" H 6100 8650 50  0001 C CNN
	1    6100 8700
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR036
U 1 1 58F51DE0
P 3150 8100
F 0 "#PWR036" H 3150 7950 50  0001 C CNN
F 1 "+12V" H 3150 8240 50  0000 C CNN
F 2 "" H 3150 8100 50  0001 C CNN
F 3 "" H 3150 8100 50  0001 C CNN
	1    3150 8100
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:CP C4
U 1 1 58F51E68
P 850 8400
F 0 "C4" H 875 8500 50  0000 L CNN
F 1 "1000u" H 875 8300 50  0000 L CNN
F 2 "" H 888 8250 50  0001 C CNN
F 3 "" H 850 8400 50  0001 C CNN
F 4 "EEUFR1V102B" H 850 8400 60  0001 C CNN "Part Number"
F 5 "Panansonic" H 850 8400 60  0001 C CNN "Manufacturer"
	1    850  8400
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:C C5
U 1 1 58F52491
P 3450 8050
F 0 "C5" H 3475 8150 50  0000 L CNN
F 1 "10n" H 3475 7950 50  0000 L CNN
F 2 "" H 3488 7900 50  0001 C CNN
F 3 "" H 3450 8050 50  0001 C CNN
	1    3450 8050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 8350 3450 8350
Wire Wire Line
	2950 8150 2950 7800
Wire Wire Line
	2950 7800 3450 7800
$Comp
L Underwaterhorn-rescue:GNDPWR #PWR037
U 1 1 58F52A09
P 850 8650
F 0 "#PWR037" H 850 8450 50  0001 C CNN
F 1 "GNDPWR" H 850 8500 50  0000 C CNN
F 2 "" H 850 8600 50  0001 C CNN
F 3 "" H 850 8600 50  0001 C CNN
	1    850  8650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR038
U 1 1 58F52A5B
P 850 8150
F 0 "#PWR038" H 850 8000 50  0001 C CNN
F 1 "+12V" H 850 8290 50  0000 C CNN
F 2 "" H 850 8150 50  0001 C CNN
F 3 "" H 850 8150 50  0001 C CNN
	1    850  8150
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:L L1
U 1 1 58F52C60
P 4100 7800
F 0 "L1" V 4050 7800 50  0000 C CNN
F 1 "15u" V 4175 7800 50  0000 C CNN
F 2 "" H 4100 7800 50  0001 C CNN
F 3 "" H 4100 7800 50  0001 C CNN
F 4 "FASTRON" V 4100 7800 60  0001 C CNN "Manufaturer"
F 5 "09HCP-150M-50" V 4100 7800 60  0001 C CNN "Part Number"
	1    4100 7800
	0    -1   -1   0   
$EndComp
Connection ~ 3450 7800
Wire Wire Line
	3450 8350 3450 8200
Wire Wire Line
	3450 7900 3450 7800
$Comp
L Underwaterhorn-rescue:CP C6
U 1 1 58F53649
P 4350 8100
F 0 "C6" H 4375 8200 50  0000 L CNN
F 1 "2200u" H 4375 8000 50  0000 L CNN
F 2 "" H 4388 7950 50  0001 C CNN
F 3 "" H 4350 8100 50  0001 C CNN
F 4 "EEUFR1V222B" H 4350 8100 60  0001 C CNN "Part Number"
F 5 "Panansonic" H 4350 8100 60  0001 C CNN "Manufacturer"
	1    4350 8100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 7750 4350 7800
Connection ~ 5300 5950
Connection ~ 5300 6600
Connection ~ 3350 7150
Wire Wire Line
	850  8150 850  8200
Wire Wire Line
	850  8550 850  8600
$Comp
L Underwaterhorn-rescue:D_Schottky D6
U 1 1 58F54827
P 3850 8100
F 0 "D6" H 3850 8200 50  0000 C CNN
F 1 "D_Schottky" H 3850 8000 50  0000 C CNN
F 2 "" H 3850 8100 50  0001 C CNN
F 3 "" H 3850 8100 50  0001 C CNN
F 4 "VISHAY" H 3850 8100 60  0001 C CNN "Manufaturer"
F 5 "MBR745" H 3850 8100 60  0001 C CNN "Part Number"
	1    3850 8100
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 7950 3850 7800
Connection ~ 3850 7800
Wire Wire Line
	3850 8250 3850 8450
Connection ~ 3850 8450
Wire Wire Line
	4750 8650 2950 8650
Connection ~ 4350 7800
NoConn ~ 2950 8750
Wire Wire Line
	900  6650 1250 6650
Wire Wire Line
	900  1750 900  2200
$Comp
L custom_addons:USB_A_2Port J1
U 1 1 58F574CC
P 5500 8150
F 0 "J1" H 5150 8600 50  0000 L CNN
F 1 "USB_A_2Port" H 5150 8500 50  0000 L CNN
F 2 "" H 5650 8200 50  0001 C CNN
F 3 "" H 5650 8200 50  0001 C CNN
	1    5500 8150
	1    0    0    -1  
$EndComp
NoConn ~ 5800 8600
NoConn ~ 5800 8500
NoConn ~ 5800 8150
NoConn ~ 5800 8050
$Comp
L Underwaterhorn-rescue:C C7
U 1 1 58F58F12
P 1250 8400
F 0 "C7" H 1275 8500 50  0000 L CNN
F 1 "220n" H 1275 8300 50  0000 L CNN
F 2 "" H 1288 8250 50  0001 C CNN
F 3 "" H 1250 8400 50  0001 C CNN
	1    1250 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  8200 1250 8200
Wire Wire Line
	1250 8200 1250 8250
Connection ~ 850  8200
Wire Wire Line
	850  8600 1250 8600
Wire Wire Line
	1250 8600 1250 8550
Connection ~ 850  8600
NoConn ~ 5350 8900
Wire Wire Line
	4250 7800 4350 7800
Wire Wire Line
	2950 8450 3850 8450
Wire Wire Line
	6100 8250 5800 8250
Wire Wire Line
	6100 8700 5800 8700
$Comp
L power:+5V #PWR039
U 1 1 58F5E4F5
P 4350 7750
F 0 "#PWR039" H 4350 7600 50  0001 C CNN
F 1 "+5V" H 4350 7890 50  0000 C CNN
F 2 "" H 4350 7750 50  0001 C CNN
F 3 "" H 4350 7750 50  0001 C CNN
	1    4350 7750
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR040
U 1 1 58F5E575
P 7050 8250
F 0 "#PWR040" H 7050 8100 50  0001 C CNN
F 1 "+5V" H 7050 8390 50  0000 C CNN
F 2 "" H 7050 8250 50  0001 C CNN
F 3 "" H 7050 8250 50  0001 C CNN
	1    7050 8250
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG041
U 1 1 58F5E5D1
P 7050 8250
F 0 "#FLG041" H 7050 8325 50  0001 C CNN
F 1 "PWR_FLAG" H 7050 8400 50  0000 C CNN
F 2 "" H 7050 8250 50  0001 C CNN
F 3 "" H 7050 8250 50  0001 C CNN
	1    7050 8250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4750 7800 4750 8650
Wire Wire Line
	4350 8450 4350 8250
$Comp
L Underwaterhorn-rescue:GNDPWR #PWR?
U 1 1 58F5E926
P 3850 8450
F 0 "#PWR?" H 3850 8250 50  0001 C CNN
F 1 "GNDPWR" H 3850 8320 50  0000 C CNN
F 2 "" H 3850 8400 50  0001 C CNN
F 3 "" H 3850 8400 50  0001 C CNN
	1    3850 8450
	1    0    0    -1  
$EndComp
$Comp
L Underwaterhorn-rescue:GNDPWR #PWR?
U 1 1 58F5E9CA
P 6100 8250
F 0 "#PWR?" H 6100 8050 50  0001 C CNN
F 1 "GNDPWR" H 6100 8120 50  0000 C CNN
F 2 "" H 6100 8200 50  0001 C CNN
F 3 "" H 6100 8200 50  0001 C CNN
	1    6100 8250
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 58F5EC1A
P 5900 7950
F 0 "#PWR?" H 5900 7800 50  0001 C CNN
F 1 "+5V" H 5900 8090 50  0000 C CNN
F 2 "" H 5900 7950 50  0001 C CNN
F 3 "" H 5900 7950 50  0001 C CNN
	1    5900 7950
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 58F5EC76
P 5900 8400
F 0 "#PWR?" H 5900 8250 50  0001 C CNN
F 1 "+5V" H 5900 8540 50  0000 C CNN
F 2 "" H 5900 8400 50  0001 C CNN
F 3 "" H 5900 8400 50  0001 C CNN
	1    5900 8400
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 7950 5900 7950
Wire Wire Line
	5800 8400 5900 8400
Wire Wire Line
	3150 8100 3150 8250
Wire Wire Line
	3150 8250 2950 8250
Wire Wire Line
	5200 1500 5000 1500
Wire Wire Line
	2900 4000 3350 4000
Wire Wire Line
	2950 6450 2950 6650
Wire Wire Line
	2950 6450 3350 6450
Wire Wire Line
	3850 4950 4400 4950
Wire Wire Line
	2950 5700 2950 5900
Wire Wire Line
	3300 4950 3850 4950
Wire Wire Line
	2900 2850 5700 2850
Wire Wire Line
	2400 1700 2500 1700
Wire Wire Line
	2400 2850 2500 2850
Wire Wire Line
	1900 2850 2400 2850
Wire Wire Line
	2400 4000 2500 4000
Wire Wire Line
	3850 5550 4400 5550
Wire Wire Line
	3850 5550 3850 5750
Wire Wire Line
	1900 1700 2400 1700
Wire Wire Line
	1900 4000 2400 4000
Wire Wire Line
	2900 1700 3350 1700
Wire Wire Line
	900  2200 900  3350
Wire Wire Line
	900  3350 900  4500
Wire Wire Line
	900  4500 900  6650
Wire Wire Line
	3450 7800 3850 7800
Wire Wire Line
	5300 5950 5600 5950
Wire Wire Line
	5300 6600 5600 6600
Wire Wire Line
	3350 7150 3750 7150
Wire Wire Line
	3850 7800 3950 7800
Wire Wire Line
	3850 8450 4350 8450
Wire Wire Line
	4350 7800 4350 7950
Wire Wire Line
	4350 7800 4750 7800
Wire Wire Line
	850  8200 850  8250
Wire Wire Line
	850  8600 850  8650
$EndSCHEMATC
